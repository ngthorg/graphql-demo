/* eslint-disable */
import moment from 'moment';
import promise from 'bluebird';
import mkdirp from 'mkdirp';
import crypto from 'crypto';
import gm from 'gm';
const imageMagick = gm.subClass({ imageMagick: true });

export default function (images, opts) {
  const options = Object.assign({}, {
    width: 960,
    height: 640,
    size: 20971520,
    quality: 0.16,
    pathTo: './public/uploads/images',
  }, opts);

  return promise.map(images, (img) => {
    return new Promise((resolve, reject) => {
      function resizeAndSave(err, size) {
        const width = size.width;
        const height = size.height;
        const totalpixel = width * height;


        if (width > options.width || height > options.height) {
          this.resize(960, 640);
        }

        if (img.buffer.length > options.size) {
          reject('err size');
        }

        if (img.size >= (totalpixel) * options.quality) {
          const encodeHQ = (1 - ((totalpixel * options.quality) / img.size)) * 100;
          this.quality(encodeHQ); // chat luong anh
        }

        const ext = `.${img.originalname.split('.').slice(-1)[0]}`;
        const filename = crypto.randomBytes(20).toString('hex') + ext;
        const today = moment().format('YYYY/MM/DD');

        function test(errors) {
          if (errors) reject(errors);
          /* save local */
          this.write(`${options.pathTo}/${today}/${filename}`, (error) => {
            if (error) {
              reject(error);
              return;
            }
            resolve(`${today}/${filename}`);
            return; // eslint-disable-line
          });
        }

        mkdirp(`${options.pathTo}/${today}`, test.bind(this));
      }

      switch (img.mimetype) {
        case 'image/jpeg':
        case 'image/png':
        case 'image/gif':
          // ImageMagick
          imageMagick(img.buffer).size(resizeAndSave);
          break;
        default:
          reject('định dạng files ko dk hỗ chợ');
          break;
      }
    });
  });
}
