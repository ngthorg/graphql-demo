import {
  connectionDefinitions,
} from 'graphql-relay';

import SearchableableType from '../types/searchable';

const {
  connectionType: SearchableConnection,
  edgeType: SearchableEdge,
} = connectionDefinitions({
  nodeType: SearchableableType,
});

export { SearchableConnection, SearchableEdge };
