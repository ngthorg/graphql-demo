import DataLoader from 'dataloader';
// import {
//   cursorToOffset,
// } from 'graphql-relay';
import {
  BoxModel,
} from '../models';


const boxById = new DataLoader((ids) => {
  return Promise.all(ids.map((id) => {
    console.log('DataLoader boxById', id);
    return BoxModel.get(id)
      .run()
      .catch(() => {
        throw new Error('ID do not exist');
      });
  }));
});

const boxs = new DataLoader((ids) => {
  return Promise.all(ids.map((id) => {
    const args = JSON.parse(id);
    const { skip = 0, first } = args;

    return BoxModel
      .orderBy('title')
      // .skip(after ? cursorToOffset(after) + 1 : 0)
      .skip(skip)
      .limit(first || 20)
      .run()
      .then((data) => {
        data.forEach((item) => { boxById.prime(item.id, item); });
        return data;
      });
  }));
});

const boxByTitle = new DataLoader((titles) => {
  return Promise.all(titles.map((title) => {
    console.log('DataLoader boxByTitle', title);
    return BoxModel
      .filter((box) => {
        return box('title').downcase().match(`.*${title}.*`);
      })
      .limit(10)
      .run()
      .then((data) => {
        data.forEach((item) => { boxById.prime(item.id, item); });
        return data;
      });
  }));
});

export default {
  getAll: boxs,
  getById: boxById,
  getBySearch: boxByTitle,
};
