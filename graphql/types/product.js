import {
  GraphQLInt,
  GraphQLString,
  GraphQLList,
  GraphQLObjectType,
} from 'graphql';

import {
  globalIdField,
} from 'graphql-relay';

import { nodeInterface } from '../relay/RelayNode';
import RelayRegistry from '../relay/RelayRegistry';

import BoxType from './box';
import ViewerType from './viewer';

// Resolver
export function productResolver(_, { id }, { loaders }) {
  return loaders.product.load(id);
}

/**
 * The GraphQL type of the ProductType
 */
const ProductType = new GraphQLObjectType({
  name: 'Product',
  description: 'This represents a Product',
  fields: () => ({
    id: globalIdField('Product'),
    name: { type: GraphQLString },
    code: { type: GraphQLString },
    price: { type: GraphQLInt },
    images: { type: new GraphQLList(GraphQLString) },
    date: { type: GraphQLString },
    box: {
      type: BoxType,
      resolve({ box, boxId }, args, { loaders }) {
        return box || loaders.box.load(boxId);
      },
    },
    viewer: {
      type: ViewerType,
      resolve() {
        return {};
      },
    },
  }),
  interfaces: [nodeInterface],
});

RelayRegistry.registerResolverForType(ProductType, productResolver);
export default RelayRegistry.registerNodeType(ProductType);
