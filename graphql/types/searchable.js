import {
  GraphQLUnionType,
} from 'graphql';

import BoxType from './box';
import ProductType from './product';


/**
 * The GraphQL type of the Search
 */
const SearchableType = new GraphQLUnionType({
  name: 'SearchableType',
  types: [BoxType, ProductType],
  resolveType(data) {
    switch (data.type) {
      case 'box':
        return BoxType;
      case 'product':
        return ProductType;
      default:
        throw new Error('object not type');
    }
  },
});

export default SearchableType;
