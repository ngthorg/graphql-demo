import fs from 'fs';
import path from 'path';
import { printSchema } from 'graphql/utilities';
import schema from '../graphql/schema';


// Save user readable type system shorthand of schema
fs.writeFileSync(
  path.join(__dirname, '../data/schema.graphql'),
  printSchema(schema),
);
