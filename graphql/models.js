/* eslint import/newline-after-import:0 */
const config = require('../config/config.thinky');
const Thinky = require('thinky')(config);
const { r, type } = Thinky;

export const BoxModel = Thinky.createModel('box', {
  id: type.string(),
  title: type.string().required(),
  type: type.string().default('box'),
});

export const ProductModel = Thinky.createModel('product', {
  id: type.string(),
  name: type.string().required(),
  code: type.string().required(),
  price: type.number().required(),
  images: type.array().schema(type.string()),
  date: type.date().default(r.now()),
  boxId: type.string().required(),
  type: type.string().default('product'),
});

export const ProfileModel = Thinky.createModel('profile', {
  id: type.string(),
  name: type.string().required(),
  email: type.string().email(),
  mobilePhone: type.string(),
  city: type.string(),
  district: type.string(),
});

export const UserModel = Thinky.createModel('user', {
  id: type.string(),
  username: type.string().min(3).max(20).required(),
  password: type.string().required(),
  avatar: type.string(),
  profileId: type.string().required(),
});

// Make sure that an index on date is available
ProductModel.ensureIndex('date');

// Join the models
BoxModel.hasMany(ProductModel, 'product', 'id', 'boxId');
ProductModel.belongsTo(BoxModel, 'box', 'boxId', 'id');

// Join the models
ProfileModel.hasOne(UserModel, 'user', 'id', 'profileId');
UserModel.hasOne(ProfileModel, 'profile', 'profileId', 'id');
