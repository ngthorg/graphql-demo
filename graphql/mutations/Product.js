import {
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLString,
  GraphQLNonNull,
} from 'graphql';

import {
  fromGlobalId,
  cursorForObjectInConnection,
  mutationWithClientMutationId,
} from 'graphql-relay';

import omit from 'lodash.omit';

import {
  ProductModel,
} from '../models';
import ProductType from '../types/product';
import { ProductEdge } from '../connections/product';
import ViewerQueries from '../queries/Viewer';


function getCursor(dataList, item) {
  for (const i of dataList) { // eslint-disable-line
    if (i.id === item.id) {
      return cursorForObjectInConnection(dataList, i);
    }
  }
  return null;
}

const ProductCreateMutation = mutationWithClientMutationId({
  name: 'ProductCreate',
  inputFields: {
    name: { type: new GraphQLNonNull(GraphQLString) },
    code: { type: new GraphQLNonNull(GraphQLString) },
    price: { type: new GraphQLNonNull(GraphQLInt) },
    boxId: { type: new GraphQLNonNull(GraphQLString) },
    images: { type: new GraphQLList(GraphQLString) },
  },
  outputFields: {
    productEdge: {
      type: ProductEdge,
      resolve(product, args, { loaders }) {
        return loaders.products.load('{}').then((products) => {
          return {
            cursor: getCursor(products, product),
            node: product,
          };
        });
      },
    },
    viewer: ViewerQueries.viewer,
  },
  mutateAndGetPayload(obj, { loaders }) {
    const inputField = omit(obj, ['clientMutationId']);
    const { id: boxId } = fromGlobalId(inputField.boxId);
    const product = Object.assign({}, inputField, { boxId, images: [] });

    return new ProductModel(product).save()
      .then((data) => {
        loaders.products.clearAll();
        loaders.productSearch.clearAll();
        loaders.product.prime(data.id, data);
        loaders.productByBoxId.clear(data.boxId);
        loaders.box.clear(data.boxId);
        return data;
      });
  },
});

const ProductRemoveMutation = mutationWithClientMutationId({
  name: 'ProductRemove',
  inputFields: {
    id: { type: new GraphQLNonNull(GraphQLID) },
  },
  outputFields: {
    deletedProductId: {
      type: GraphQLID,
      resolve: ({ id }) => {
        return id;
      },
    },
    viewer: ViewerQueries.viewer,
  },
  mutateAndGetPayload({ id }, { loaders }) {
    const { id: localProductId } = fromGlobalId(id);

    return loaders.product.load(localProductId)
      .then((res) => {
        return res.delete().then((item) => {
          loaders.products.clearAll();
          loaders.productSearch.clearAll();
          loaders.product.clear(localProductId);
          loaders.productByBoxId.clear(item.boxId);
          loaders.box.clear(item.boxId);
          return Object.assign({}, item, { id });
        });
      });
  },
});

const ProductUpdateMutation = mutationWithClientMutationId({
  name: 'ProductUpdate',
  inputFields: {
    id: { type: new GraphQLNonNull(GraphQLID) },
    name: { type: new GraphQLNonNull(GraphQLString) },
    code: { type: new GraphQLNonNull(GraphQLString) },
    price: { type: new GraphQLNonNull(GraphQLInt) },
  },
  outputFields: {
    product: {
      type: ProductType,
      resolve(product) {
        return product;
      },
    },
  },
  mutateAndGetPayload(obj, { loaders }) {
    const { id } = fromGlobalId(obj.id);

    return loaders.product.load(id)
      .then((data) => {
        // data.name = obj.name;
        return Object.assign(data, { ...omit(obj, ['id', 'clientMutationId']) }).save();
      })
      .then((data) => {
        loaders.products.clearAll();
        loaders.productSearch.clearAll();
        loaders.product.clear(id).prime(id, data);
        loaders.productByBoxId.clear(data.boxId);
        loaders.box.clear(data.boxId);
        return data;
      });
  },
});

export default {
  create: ProductCreateMutation,
  remove: ProductRemoveMutation,
  update: ProductUpdateMutation,
};
