// import expect from 'expect';
// import { describe, it } from 'mocha';
// import {
//   GraphQLObjectType,
// } from 'graphql';
// import { globalIdField } from 'graphql-relay';
// import RelayRegistry from '../RelayRegistry';
//
// const UserType = new GraphQLObjectType({
//   name: 'User',
//   description: 'An authenticated user of the application.',
//   fields: () => ({
//     id: globalIdField('User'),
//   }),
// });
//
// describe('RelayRegistry', () => {
//   it('should set and NodeType', () => {
//     RelayRegistry.registerNodeType(UserType);
//
//     expect(RelayRegistry.getNodeForTypeName(UserType.name)).toEqual(UserType);
//   });
//
//   it('should set and ResolverForType', (done) => {
//     const data = { name: 'ahihi' };
//     function ResolverDemo() {
//       return Promise.resolve(data);
//     }
//     RelayRegistry.registerResolverForType(UserType, ResolverDemo);
//     RelayRegistry.getResolverForNodeType(UserType.name)().then((res) => {
//       expect(res).toEqual({
//         ...data,
//         __relayType: UserType.name.toLowerCase(),
//       });
//     })
//     .then(done)
//     .catch(done);
//   });
// });
