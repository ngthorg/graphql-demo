import {
  GraphQLInt,
  GraphQLString,
  GraphQLObjectType,
} from 'graphql';

import {
  globalIdField,
} from 'graphql-relay';

import { nodeInterface } from '../relay/RelayNode';
import RelayRegistry from '../relay/RelayRegistry';

import BoxQueries from '../queries/Box';
import ProductQueries from '../queries/Product';
import SearchableQueries from '../queries/Searchable';
import ViewerQueries from '../queries/Viewer';

// Resolver
export function viewerResolver(_, { id }) {
  console.log('viewerResolver', id);
  return {};
}

const EmailType = new GraphQLObjectType({
  name: 'Email',
  description: 'This represents a Me',
  fields: () => ({
    address: { type: GraphQLString },
  }),
});

const PhoneType = new GraphQLObjectType({
  name: 'Phone',
  description: 'This represents a Me',
  fields: () => ({
    number: { type: GraphQLString },
    country_prefix: { type: GraphQLString },
    national_number: { type: GraphQLString },
  }),
});

const Viewer = new GraphQLObjectType({
  name: 'Viewer',
  description: 'This represents a Me',
  fields: () => ({
    id: globalIdField('Viewer'),
    username: {
      type: GraphQLString,
    },
    phone: {
      type: PhoneType,
      resolve(data) {
        return data.phone;
      },
    },
    email: {
      type: EmailType,
      resolve(data) {
        return data.email;
      },
    },
    password: {
      type: GraphQLString,
    },
    accessToken: {
      type: GraphQLString,
      resolve(data) {
        return data.access_token;
      },
    },
    expires: {
      type: GraphQLInt,
      resolve(data) {
        return data.token_refresh_interval_sec;
      },
    },
    boxs: BoxQueries.boxs,
    products: ProductQueries.products,
    searchs: SearchableQueries.searchables,
    viewers: ViewerQueries.viewers,
    // my: {
    //   type: UserType,
    //   resolve(data) {
    //     console.log('my', data);
    //     return UserModel
    //       .getJoin({ profile: true })
    //       .filter({
    //         profile: {
    //           email: data.email.address,
    //         },
    //       })
    //       .run()
    //       .then((res) => {
    //         return res[0] || null;
    //       });
    //   },
    // },
  }),
  interfaces: [nodeInterface],
});

RelayRegistry.registerResolverForType(Viewer, viewerResolver);
export default RelayRegistry.registerNodeType(Viewer);
