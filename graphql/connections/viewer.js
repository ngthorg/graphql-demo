import {
  connectionDefinitions,
} from 'graphql-relay';

import ViewerType from '../types/viewer';

const {
  connectionType: ViewerConnection,
  edgeType: ViewerEdge,
} = connectionDefinitions({
  nodeType: ViewerType,
});

export { ViewerConnection, ViewerEdge };
