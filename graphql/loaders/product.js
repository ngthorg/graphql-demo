import DataLoader from 'dataloader';
import {
  cursorToOffset,
} from 'graphql-relay';
import {
  ProductModel,
} from '../models';

import boxLoaders from './box';


const productById = new DataLoader((ids) => {
  return Promise.all(ids.map((id) => {
    console.log('DataLoader productById', id);
    return ProductModel.get(id)
      .getJoin({ box: true })
      .run()
      .then((data) => {
        boxLoaders.getById.prime(data.boxId, data.box);
        return data;
      })
      .catch(() => {
        throw new Error('ID do not exist');
      });
  }));
});

const products = new DataLoader((ids) => {
  return Promise.all(ids.map((id) => {
    const args = JSON.parse(id);
    const { after, first } = args;

    return ProductModel
      .getJoin({ box: true })
      .orderBy('title')
      .skip(after ? cursorToOffset(after) + 1 : 0)
      .limit(first || 20)
      .run()
      .then((data) => {
        data.forEach((item) => {
          productById.prime(item.id, item);
          boxLoaders.getById.prime(item.boxId, item.box);
        });
        return data;
      });
  }));
});

const productsByBoxId = new DataLoader((ids) => {
  return Promise.all(ids.map((id) => {
    console.log('DataLoader productsByBoxId', id);
    return ProductModel
      .filter({ boxId: id })
      .run()
      .then((data) => {
        data.forEach((item) => {
          productById.prime(item.id, item);
        });
        return data;
      });
  }));
});

const productByName = new DataLoader((names) => {
  return Promise.all(names.map((name) => {
    return ProductModel
      .filter((product) => {
        return product('name').downcase().match(`.*${name}.*`);
      })
      .limit(10)
      .run();
  }));
});

export default {
  getAll: products,
  getById: productById,
  getBySearch: productByName,
  getByBoxId: productsByBoxId,
};
