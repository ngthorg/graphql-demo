import {
  GraphQLID,
  GraphQLString,
  GraphQLNonNull,
} from 'graphql';
import {
  fromGlobalId,
  connectionArgs,
  connectionFromPromisedArray,
} from 'graphql-relay';

import ProductType from '../types/product';
import { ProductConnection } from '../connections/product';

export default {
  product: {
    type: ProductType,
    args: {
      id: {
        type: new GraphQLNonNull(GraphQLID),
      },
    },
    resolve(root, { id }, { loaders }) {
      const { id: productId } = fromGlobalId(id);

      return loaders.product.load(productId);
    },
  },
  products: {
    type: ProductConnection,
    args: {
      boxId: {
        type: GraphQLString,
      },
      ...connectionArgs,
    },
    resolve(root, { boxId, ...args }, { loaders }) {
      console.log('args', args);
      if (boxId) {
        const { id } = fromGlobalId(boxId);
        return connectionFromPromisedArray(loaders.productByBoxId.load(id), args);
      }

      return connectionFromPromisedArray(loaders.products.load(
        JSON.stringify(args),
      ), {});
    },
  },
};
