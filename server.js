import express from 'express';
import cors from 'cors';
import graphHTTP from 'express-graphql';
import schema from './graphql/schema';
import loaders from './graphql/loaders';

const APP_PORT = process.env.APP_PORT || 3000;

const app = express();

app.use(cors());

// Serve static resources
app.use('/graphql', graphHTTP(request => ({
  schema,
  pretty: true,
  graphiql: true,
  context: { loaders },
  rootValue: { authorization: request.headers.authorization },
})));

app.listen(APP_PORT, () => {
  console.log(`GraphQL is listening on port ${APP_PORT}`);
});
