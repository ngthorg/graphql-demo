import {
  GraphQLString,
  GraphQLObjectType,
} from 'graphql';

import {
  connectionArgs,
  globalIdField,
  connectionFromPromisedArray,
} from 'graphql-relay';

import ViewerType from './viewer';

import { nodeInterface } from '../relay/RelayNode';
import RelayRegistry from '../relay/RelayRegistry';
import { ProductConnection } from '../connections/product';

// Resolver
export function boxResolver(_, { id }, { loaders }) {
  return loaders.box.load(id);
}

/**
 * The GraphQL type of the BoxType
 */
const BoxType = new GraphQLObjectType({
  name: 'Box',
  description: 'This represents a Box',
  fields: () => ({
    id: globalIdField('Box'),
    title: { type: GraphQLString },
    products: {
      type: ProductConnection,
      args: {
        ...connectionArgs,
      },
      resolve(obj, args, { loaders }) {
        return connectionFromPromisedArray(loaders.productByBoxId.load(obj.id), args);
      },
    },
    viewer: {
      type: ViewerType,
      resolve() {
        return {};
      },
    },
  }),
  interfaces: [nodeInterface],
});

RelayRegistry.registerResolverForType(BoxType, boxResolver);
export default RelayRegistry.registerNodeType(BoxType);
