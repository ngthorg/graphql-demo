import {
  GraphQLID,
  GraphQLInt,
  GraphQLNonNull,
} from 'graphql';
import {
  fromGlobalId,
  connectionArgs,
  connectionFromPromisedArray,
} from 'graphql-relay';

import BoxType from '../types/box';
import { BoxConnection } from '../connections/box';

export default {
  box: {
    type: BoxType,
    args: {
      id: {
        type: new GraphQLNonNull(GraphQLID),
      },
    },
    resolve(_, { id }, { loaders }) {
      const { id: boxId } = fromGlobalId(id);

      return loaders.box.load(boxId);
    },
  },
  boxs: {
    type: BoxConnection,
    args: {
      skip: {
        type: GraphQLInt,
        defaultValue: 0,
      },
      ...connectionArgs,
    },
    resolve(_, args, { loaders }) {
      console.log('args', args);
      return connectionFromPromisedArray(loaders.boxs.load(JSON.stringify(args)), {});
    },
  },
};
