import {
  GraphQLSchema,
  GraphQLObjectType,
} from 'graphql';

import { nodeField } from './relay/RelayNode';

import ViewerQueries from './queries/Viewer';
import BoxQueries from './queries/Box';
import ProductQueries from './queries/Product';

import BoxMutation from './mutations/Box';
import ProductMutation from './mutations/Product';

const Query = new GraphQLObjectType({
  name: 'Query',
  description: 'This is a root Query',
  fields: () => ({
    node: nodeField,
    viewer: ViewerQueries.viewer,
    box: BoxQueries.box,
    product: ProductQueries.product,
  }),
});

const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  description: 'This is a root Mutation',
  fields: {
    createBox: BoxMutation.create,
    updateBox: BoxMutation.update,
    removeBox: BoxMutation.remove,
    createProduct: ProductMutation.create,
    updateProduct: ProductMutation.update,
    removeProduct: ProductMutation.remove,
  },
});

const Schema = new GraphQLSchema({
  query: Query,
  mutation: Mutation,
});

export default Schema;
