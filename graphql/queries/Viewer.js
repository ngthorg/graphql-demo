import {
  connectionArgs,
  connectionFromArray,
} from 'graphql-relay';
// import crypto from 'crypto';
// import Axios from 'axios';

// import accountKitConfig from '../../config/account-kit';
import ViewerType from '../types/viewer';

import { ViewerConnection } from '../connections/viewer';

export default {
  viewer: {
    type: ViewerType,
    resolve({ authorization }) {
      console.log('authorization', authorization);
      // let tonken;
      // if (authorization && authorization.split(' ')[0] === 'Bearer') {
      //   tonken = authorization.split(' ')[1];
      // }
      //
      // if (tonken) {
      //   const hash = crypto.createHmac('sha256', accountKitConfig.APPSECRET)
      //                       .update(tonken)
      //                       .digest('hex');
      //   console.log('hash', hash);
      //
      //   return Axios.get(accountKitConfig.ME_ENDPOINT_BASE_URL, {
      //     params: {
      //       access_token: tonken,
      //       appsecret_proof: hash,
      //     },
      //   })
      //   .then((res) => {
      //     console.log('me', res.data);
      //     return res.data;
      //   })
      //   .catch(() => {
      //     return { };
      //   });
      // }

      return { };
    },
  },
  viewers: {
    type: ViewerConnection,
    args: {
      ...connectionArgs,
    },
    resolve(_, args) {
      return connectionFromArray([{}], args);
    },
  },
};
