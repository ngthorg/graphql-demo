import boxLoader from './box';
import productLoader from './product';


export default {
  boxs: boxLoader.getAll,
  box: boxLoader.getById,
  boxSearch: boxLoader.getBySearch,

  products: productLoader.getAll,
  product: productLoader.getById,
  productSearch: productLoader.getBySearch,
  productByBoxId: productLoader.getByBoxId,
};
