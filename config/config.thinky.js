
module.exports = {
  development: {
    host: 'localhost',
    port: 28015,
    db: 'graphql_demo',
  },
  production: {
    host: 'rethinkdb',
    port: 28015,
    db: 'graphql_demo',
  },
}[process.env.NODE_ENV || 'development'];
