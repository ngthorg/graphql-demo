
module.exports = {
  development: {
    APPID: '124825337927566',
    APPSECRET: 'ca0ea322203b3c0d82fa3c28581b4abb',
    STATE: '1234',
    VERSION: 'v1.0',
    BASE_URL: 'https://graph.accountkit.com/v1.0',
    ME_ENDPOINT_BASE_URL: 'https://graph.accountkit.com/v1.0/me',
    TOKEN_EXCHANGE_BASE_URL: 'https://graph.accountkit.com/v1.0/access_token',
  },
  production: {
    APPID: '124825337927566',
    APPSECRET: 'ca0ea322203b3c0d82fa3c28581b4abb',
    STATE: '1234',
    VERSION: 'v1.0',
    BASE_URL: 'https://graph.accountkit.com/v1.0',
    ME_ENDPOINT_BASE_URL: 'https://graph.accountkit.com/v1.0/me',
    TOKEN_EXCHANGE_BASE_URL: 'https://graph.accountkit.com/v1.0/access_token',
  },
}[process.env.NODE_ENV || 'development'];
