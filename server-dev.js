import express from 'express';
import cors from 'cors';
import multer from 'multer';
import graphHTTP from 'express-graphql';
import schema from './graphql/schema';
import loaders from './graphql/loaders';
import updataImages from './updataImages';

const GRAPHQL_PORT = process.env.GRAPHQL_PORT || 8000;

const app = express();

app.use(cors());

const storage = multer.memoryStorage();
const multerMiddleware = multer({ storage }).fields([{ name: 'image', maxCount: 6, minCount: 2 }]);

app.use('/graphql', multerMiddleware, (req, res, next) => {
  if (!req.files || req.files.length === 0) {
    next();
    return;
  }
  updataImages(req.files.image, {
    width: 960,
    height: 640,
    size: 20971520,
    quality: 0.16,
    pathTo: './public/uploads/images',
  })
    .then((images) => {
      // req.images = images;
      res.send(images);
      // next();
    })
    .catch((err) => {
      console.log(err);
      res.send('catch');
      // next();
    });
});

app.use('/graphql', graphHTTP(request => ({
  schema,
  graphiql: true,
  pretty: true,
  context: { loaders },
  rootValue: { authorization: request.headers.authorization },
})));

app.listen(GRAPHQL_PORT, () => {
  console.log(`GraphQL is listening on port ${GRAPHQL_PORT}`);
});
