import {
  GraphQLID,
  GraphQLString,
  GraphQLNonNull,
} from 'graphql';

import {
  fromGlobalId,
  cursorForObjectInConnection,
  mutationWithClientMutationId,
} from 'graphql-relay';

import BoxType from '../types/box';
import { BoxEdge } from '../connections/box';
import ViewerQueries from '../queries/Viewer';

import {
  BoxModel,
} from '../models';

function getCursor(dataList, item) {
  for (const i of dataList) { // eslint-disable-line
    if (i.id === item.id) {
      return cursorForObjectInConnection(dataList, i);
    }
  }
  return null;
}

const BoxCreateMutation = mutationWithClientMutationId({
  name: 'BoxCreate',
  inputFields: {
    title: { type: new GraphQLNonNull(GraphQLString) },
  },
  outputFields: {
    boxEdge: {
      type: BoxEdge,
      resolve(box, args, { loaders }) {
        return loaders.boxs.load('{}').then((boxs) => {
          return {
            cursor: getCursor(boxs, box),
            node: box,
          };
        });
      },
    },
    viewer: ViewerQueries.viewer,
  },
  mutateAndGetPayload({ title }, { loaders }) {
    return new BoxModel({ title }).save()
      .then((data) => {
        console.log('then', data);
        loaders.boxs.clearAll();
        loaders.boxSearch.clearAll();
        loaders.box.prime(data.id, data);
        return data;
      });
  },
});

const BoxRemoveMutation = mutationWithClientMutationId({
  name: 'BoxRemove',
  inputFields: {
    id: { type: new GraphQLNonNull(GraphQLID) },
  },
  outputFields: {
    deletedBoxId: {
      type: GraphQLID,
      resolve: ({ id }) => {
        return id;
      },
    },
    viewer: ViewerQueries.viewer,
  },
  mutateAndGetPayload({ id }, { loaders }) {
    const { id: localBoxId } = fromGlobalId(id);

    return loaders.box.load(localBoxId)
      .then((res) => {
        return res.delete().then((item) => {
          loaders.boxs.clearAll();
          loaders.boxSearch.clearAll();
          loaders.box.clear(localBoxId);
          return Object.assign({}, item, { id });
        });
      });
  },
});

const BoxUpdateMutation = mutationWithClientMutationId({
  name: 'BoxUpdate',
  inputFields: {
    id: { type: new GraphQLNonNull(GraphQLID) },
    title: { type: new GraphQLNonNull(GraphQLString) },
  },
  outputFields: {
    box: {
      type: BoxType,
      resolve(box) {
        return box;
      },
    },
  },
  mutateAndGetPayload(input, { loaders }) {
    const { id } = fromGlobalId(input.id);

    return loaders.box.load(id)
      .then((data) => {
        data.title = input.title;
        return data.save();
      })
      .then((data) => {
        loaders.boxs.clearAll();
        loaders.boxSearch.clearAll();
        loaders.box.clear(id).prime(data.id, data);
        return data;
      });
  },
});

export default {
  create: BoxCreateMutation,
  remove: BoxRemoveMutation,
  update: BoxUpdateMutation,
};
