import {
  GraphQLString,
  GraphQLNonNull,
} from 'graphql';

import {
  connectionArgs,
  connectionFromPromisedArray,
} from 'graphql-relay';

import { SearchableConnection } from '../connections/searchable';


export default {
  searchables: {
    type: SearchableConnection,
    args: {
      text: {
        type: new GraphQLNonNull(GraphQLString),
      },
      ...connectionArgs,
    },
    resolve(_, { text, ...args }, { loaders }) {
      const textSearch = text.toLowerCase();

      return connectionFromPromisedArray(
        Promise.all([
          loaders.boxSearch.load(textSearch),
          loaders.productSearch.load(textSearch),
        ])
        .then(dataArrays => [].concat(...dataArrays)),
        args,
      );
    },
  },
};
